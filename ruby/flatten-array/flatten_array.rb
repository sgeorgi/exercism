class FlattenArray
  def self.flatten(array)
    array.flatten.reject {|a| a.nil? }
  end
end