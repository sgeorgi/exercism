import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit

class Gigasecond(private val dateTime: LocalDateTime) {
  constructor(date: LocalDate) : this(LocalDateTime.of(date, LocalTime.MIDNIGHT))

  val date: LocalDateTime
    get() = dateTime.plus(1_000_000_000, ChronoUnit.SECONDS)
}