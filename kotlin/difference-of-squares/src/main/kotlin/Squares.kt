import kotlin.math.pow

class Squares(private val number: Int) {
  private infix fun Int.pow(exponent: Int): Int = toDouble().pow(exponent).toInt()

  fun squareOfSum() = (1..number).sum() pow 2

  fun sumOfSquares(): Int = (1..number).map { it pow 2 }.sum()

  fun difference(): Int = squareOfSum() - sumOfSquares()
}