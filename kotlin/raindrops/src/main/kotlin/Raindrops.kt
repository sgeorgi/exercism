object Raindrops {
  fun convert(drops: Int): String {
    val factors = ArrayList<Int>()
    val output = ArrayList<String>()

    for (i in 1..drops) {
      if (drops % i == 0) factors.add(i)
    }

    for (i in factors) {
      when (i) {
        3 -> output.add("Pling")
        5 -> output.add("Plang")
        7 -> output.add("Plong")
      }
    }

    if (output.isEmpty()) {
      output.add(factors.last().toString())
    }

    return output.joinToString("")
  }
}