object Hamming {
  fun compute(s1: String, s2: String): Int {
    require(s1.length == s2.length) { "left and right strands must be of equal length." }

    return (0 until s1.length).count { s1[it] != s2[it] }
  }
}