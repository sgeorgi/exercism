object Pangram {
  fun isPangram(text: String): Boolean = ('a'..'z').all { text.toLowerCase().contains(it) }
}